[[ $- != *i* ]] && return

PROMPT_COMMAND=PS1_PROMPT
PS1_PROMPT() {
	EC=$?  # saved error-code
	PS1="\[\e[34m\]\w\[\e[0m\] "

	[ $EC -ne 0 ] && PS1+="\[\e[31m\][$EC]\[\e[0m\] "

	PS1+="$ "
}

fsize() { numfmt --to=iec $(stat -c %s "$1"); }
mkcdir() { mkdir -p "$1"; cd "$1"; }
launch() { nohup $@ 2>/dev/null & }

alias size="stat -c %s"
alias ffmpeg="ffmpeg -hide_banner"
alias ffplay="ffplay -hide_banner"
alias ffprobe="ffprobe -hide_banner"
alias fmtof="ffprobe -select_streams 0 -v error -show_entries format=format_long_name -of default=nk=1:nw=1"

alias xcopy="xclip -sel clip"
alias xpaste="xclip -o -sel clip"
