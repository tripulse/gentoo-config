The directory tree is relative to root, which effectively is absolute (with some exceptions).
No I didn't use `stow` for *managing*™ my configuration files (you may call them *dotfiles*),
why bother to do that?

- `~` means the `$HOME` folder, which is usually `/home/yourname`.
